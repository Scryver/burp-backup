FROM debian:jessie-backports

# add our user and group first to make sure their IDs get assigned consistently, regardless of whatever dependencies get added
# RUN groupadd -r burpy && useradd -r -g burpy burpy

# add gosu for easy step-down from root
# ENV GOSU_VERSION 1.7
# RUN set -x \
#     && apt-get update && apt-get install -y --no-install-recommends ca-certificates wget && rm -rf /var/lib/apt/lists/* \
#     && wget -O /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$(dpkg --print-architecture)" \
#     && wget -O /usr/local/bin/gosu.asc "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$(dpkg --print-architecture).asc" \
#     && export GNUPGHOME="$(mktemp -d)" \
#     && gpg --keyserver ha.pool.sks-keyservers.net --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4 \
#     && gpg --batch --verify /usr/local/bin/gosu.asc /usr/local/bin/gosu \
#     && rm -r "$GNUPGHOME" /usr/local/bin/gosu.asc \
#     && chmod +x /usr/local/bin/gosu \
#     && gosu nobody true \
#     && apt-get purge -y --auto-remove ca-certificates wget

RUN set -x \
    && apt-get update \
    && apt-get install -t jessie-backports -y \
        burp \
        openssl \
    && rm -rf /var/lib/apt/lists/*

# RUN mkdir /backup \
#     && mkdir -p /var/run/burp \
#     && chmod 777 /var/run/burp
    # && chown -R burpy:burpy /backup /var/run/burp /etc/burp \

# @TODO switch to protocol1
RUN sed -i 's/# keep = 4/keep = 4/' /etc/burp/burp-server.conf \
    && sed -i 's/# keep = 6/keep = 12\nkeep = 50/' /etc/burp/burp-server.conf \
    && sed -i 's/#status_address = 127.0.0.1/status_address = 0.0.0.0/' /etc/burp/burp-server.conf \
    && sed -i 's/#address = 0.0.0.0/address = 0.0.0.0/' /etc/burp/burp-server.conf \
    && sed -i 's/stdout = 0/stdout = 1/' /etc/burp/burp-server.conf \
    && rm /etc/burp/clientconfdir/testclient
    # && sed -i 's|directory = /var/spool/burp|directory = /backup|' /etc/burp/burp-server.conf \
    # && sed -i 's|pidfile = /var/run/burp.server.pid|pidfile = /var/run/burp/server.pid|' /etc/burp/burp-server.conf \
    # && sed -i 's/# user=graham/user=burpy/' /etc/burp/burp-server.conf \
    # && sed -i 's/# group=nogroup/group=burpy/' /etc/burp/burp-server.conf

VOLUME /var/spool/burp /etc/burp

COPY docker-entrypoint.sh /usr/local/bin/
# RUN chmod +x /usr/local/bin/docker-entrypoint.sh
ENTRYPOINT ["docker-entrypoint.sh"]

EXPOSE 4971 4972
CMD ["burp"]
