#!/bin/bash

# set Errors on fail at first command and with Option of pipe-failures, Unbound
# variables are also treated as errors, print all commands before eXecuting.
# set -euxo pipefail
set -eo pipefail

# if command starts with an option, prepend burp
if [ "${1:0:1}" = '-' ]; then
    set -- burp "$@"
fi

# if there are no arguments specified, set default server action
if [ "$#" -eq 0 ]; then
    exec -c /etc/burp/burp-server.conf -F
# if the only argument specified is burp, set default server action
elif [ "$#" -eq 1 -a "$1" = "burp" ]; then
    exec burp -c /etc/burp/burp-server.conf -F
elif [ "$1" = "burp" ]; then
    exec "$@"
fi


exec "$@"
