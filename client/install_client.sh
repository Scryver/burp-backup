#!/bin/bash

function installer {
	lsb_release --id | grep Debian
	if [ $? = 0 ]; then
		sudo apt install -t jessie-backports burp
	fi

	lsb_release --id | grep Ubuntu
	if [ $? = 0 ]; then
		echo "Install according to https://ziirish.info/debian/README.txt"
		exit 1
	fi
}

copy_config() {
	sed "s/server = 127.0.0.1/server = $(docker inspect --format '{{ .NetworkSettings.IPAddress }}' burpy)/" burp.conf > burp_client.conf
	if [ "$1" = "-u" ]; then
		user="$2"
	else
		user="generic"
	fi
	if [ "$3" = "-p" ]; then
		pass="$4"
	else
		pass="really_stupid_to_use_this"
	fi
	sed "s/cname = testclient/cname = $user/" burp_client.conf > burp_client2.conf
	sed "s/password = abcdefgh/password = $pass/" burp_client2.conf > burp_client.conf
	rm burp_client2.conf
	sudo mv burp_client.conf /etc/burp/burp.conf
}

function edit_cron {
	echo "Uncomment the client settings in the following file"
	read -n1 -r -p "Press key to continue..." key
	sudo nano /etc/cron.d/burp
}

function install_client {
	sudo cp clientname.client /var/lib/docker/volumes/backup_conf/_data/clientconfdir/clientname
}

if [ "$1" = "install" ]; then
	installer
	copy_config $2 $3 $4 $5
	edit_cron
	install_client

elif [ "$1" = "update-config" ]; then
	copy_config $2 $3 $4 $5

elif [ "$1" = "update-client" ]; then
	install_client

else
	echo "Usage:"
	echo ""
	echo "$0 install 		: install burp and setup whole stack of config"
	echo "$0 update-config	: update config, also sets current docker ip in config"
	echo "$0 update-client 	: update burp client file in docker server"

	echo "$0 COMMAND -u $USER -p $PASS  : for user/password setting of client"
fi
