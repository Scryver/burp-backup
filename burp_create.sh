#!/bin/bash

if [ "$1" = "build" ]; then
    docker build -t burp:latest .

elif [ "$1" = "create-volumes" ]; then
    docker volume create backup_conf
    docker volume create backup_data

elif [ "$1" = "recreate-config" ]; then
    docker volume rm backup_conf
    docker volume create backup_conf

elif [ "$1" = "run" ]; then
    docker run --restart always \
        --name burpy \
        -dit \
        -v backup_data:/var/spool/burp \
        -v backup_conf:/etc/burp \
        -p 4971:4971 \
        -p 4972:4972 \
        burp:latest

elif [ "$1" = "run-local" ]; then
    docker run --restart always \
        --name burpy \
        -dit \
        -v backup_data:/var/spool/burp \
        -v backup_conf:/etc/burp \
        -p 127.0.0.1:4971:4971 \
        -p 127.0.0.1:4972:4972 \
        burp:latest

elif [ "$1" = "backup" ]; then
    docker stop burpy
    docker run --rm -v backup_data:/data:ro -v /backup:/backup debian:jessie-backports tar -czf /backup/server-$(date -I).tar.gz /data
    docker start burpy

elif [ "$1" = "restore" ]; then
    echo "This command will destroy the current backup_data volume!"
    read -n1 -r -p "Press any key to stop, press 'u' to continue" key
    if [ "$key" = "u" ]; then
        docker stop burpy
        docker run --rm -v backup_data:/data -v /backup:/backup:ro debian:jessie-backports bash -c "cd /data && rm -rf * && rm -rf .* && tar -xzf /backup/server-restore.tar.gz --strip 1"

        docker start burpy
    fi

else
    echo "Usage:"
    echo ""
    echo "$0 build              : build this image and tag it as latest"
    echo "$0 create-volumes     : create docker volumes for config and data"
    echo "$0 recreate-config    : remove docker backup_conf volume and recreates it for the running container to fill"
    echo "$0 run                : run the docker burp container and serve globally (0.0.0.0)"
    echo "$0 run-local          : run the docker burp container and serve local (127.0.0.1)"
    echo "$0 backup             : backup data from backup_data docker volume to /backup/server-DATE.tar.gz"
    echo "$0 restore            : restore data from /backup/server-restore.tar.gz to the backup_data docker volume"
fi
